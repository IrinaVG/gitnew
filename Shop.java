package com.company;

public class Shop {

	private String name;
	private int size;
	private String sex;
	private String type;
	private String collection;
	private boolean isInStoke;
	private int price;

	public Shop(String name) {
		this.name = name;
		this.size = size;
		this.sex = sex;
		this.type = type;
		this.collection = collection;
		this.isInStoke = true;
		this.price = price;
	}

	public void tryOn(int name) {
		System.out.println("Do you want to try " + this.name + " on from " + collection + " (" + this.size + " " + this.sex + ") ?");
		System.out.println("Do you like it?");
		String answer = scaner.nextLine();

		if (answer = "Yes") {
			Buy(this.name);
		}
	}

	public void Buy(int name) {
		System.out.println("Do you want to buy " + this.name + " from " + collection + " (" + this.size + " " + this.sex + ") ?");
		String answer = scaner.nextLine();
		if (answer = "Yes") {
			isInStoke = false;
			System.out.println(this.name + " has been bought");
		}
	}


	@java.lang.Override
	public java.lang.String toString() {
		return "Shop{" +
				"name='" + name + '\'' +
				", size=" + size +
				", sex='" + sex + '\'' +
				", type='" + type + '\'' +
				", collection='" + collection + '\'' +
				", isInStoke=" + isInStoke +
				", price=" + price +
				'}';
	}

	public String getName() {
		return name;
	}

	public int getSize() {
		return size;
	}

	public String getSex() {
		return sex;
	}

	public String getType() {
		return type;
	}

	public String getCollection() {
		return collection;
	}

	public boolean isInStoke() {
		return isInStoke;
	}

	public int getPrice() {
		return price;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public void setInStoke(boolean inStoke) {
		isInStoke = inStoke;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}